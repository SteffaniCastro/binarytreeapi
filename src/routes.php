<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->group('/api', function() use ($app) {
    $app->group('/v1', function() use ($app) {
        $app->post('/createTree', '\App\Controllers\TreeController:createTree');
        $app->get('/lca/{id_tree}/{node1}/{node2}', '\App\Controllers\TreeController:lowest_common_ancestor');
    });

});
