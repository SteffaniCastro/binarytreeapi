<?php
$dotenv = new \Dotenv\Dotenv(__DIR__ . '/../');
$dotenv->load();

$mode = $_ENV['APP_MODE'];
ini_set ('memory_limit', '-1'); 
return [
    'settings' => [
        'mode' => $mode,
        'debug' => $mode == 'development' ? true : false,
        'displayErrorDetails' => $mode == 'development' ? true : false,

        'log.enabled' => true,
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header.

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'Binary Tree',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        // Database settings
        'db' => [
            'driver' => $_ENV['DB_DRIVER'],
            'host' => $_ENV['DB_HOST'],
            'database' => $_ENV['DB_DATABASE'],
            'username' => $_ENV['DB_USERNAME'],
            'password' => $_ENV['DB_PASSWORD'],
            'charset'   => 'utf8', 
        ],
    ],
];
