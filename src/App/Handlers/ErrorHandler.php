<?php
namespace App\Handlers;

class ErrorHandler 
{
    protected $logger;
    public function __construct($logger)
    {
        $this->logger = $logger;
    }

    public function __invoke($request, $response, $exception) 
    {
        $this->logger->error($exception);
        $resp = Array('result' => $exception, 'error' => 'Internal error in server.');
        return $response
            ->withJson($resp, 500);
    }
}

