<?php
namespace App\Controllers;

use App\Models\Tree;

class TreeController {

    public $treeFinal;
    public $levels = array();
    

    public function __construct($container){
        $this->container = $container;
        $this->logger    = $container->logger;
    }

	public function createTree (\Slim\Http\Request $request, \Slim\Http\Response $response){

        $nodes = explode(",",$request->getParsedBody()['nodos']);    

        if (count($nodes)<2){
             $resp = array('result' => null, 'error' => 'Invalid nodes supplied.');
             return $response->withJson($resp, 400);
        }

        $tree = Tree::all();
        $treelast = $tree->last();

            if($treelast == null)
                $treeId = 1;
            else 
                $treeId = $treelast->id_tree + 1;
            
        foreach ($nodes  as $value) {

            $node = Tree::where([['root','=', 1],
                                 ['id_tree','=',$treeId]])
                          ->get();

            if (count($node) == 0){//insert root
                $this->treeFinal = '';

                $tree = new Tree;
                $tree->id_tree = $treeId;
                $tree->node = $value;
                $tree->father = '';
                $tree->child_left = '';
                $tree->child_rigth = '';
                $tree->root = 1;
                $tree->save(); 
            } else{

                $recor = $node[0]->node;  
                $father = null;
                $band = true;

                while($band){
                    $father = $recor;

                    $nodeNew = Tree::where([['node','=',$recor],
                                            ['id_tree','=',$treeId]])->get();

                        
                    if($value < $recor){//left
                        if($nodeNew[0]->child_left == ''){//no tiene nodo a la izq
                            $band = false;
                        } 
                        $recor = $nodeNew[0]->child_left;
                    
                    } else {//rigth
                        if($nodeNew[0]->child_rigth == ''){//no tiene nodo a la izq
                            $band = false;
                        } 
                        $recor = $nodeNew[0]->child_rigth;
                        
                    }
                }//while

                if($value < $father){
                    $updateRowNode = Tree::where('id', $nodeNew[0]->id)->update(['child_left' => $value]);

                    if($updateRowNode == 0){
                        $resp = array('result' => null, 'error' => 'Internal server error.');
                        return $response->withJson($resp, 500);  
                    }
                    
                } else {
                    $updateRowNode = Tree::where('id', $nodeNew[0]->id)->update(['child_rigth' => $value]);

                    if($updateRowNode == 0){
                        $resp = array('result' => null, 'error' => 'Internal server error.');
                        return $response->withJson($resp, 500);  
                    }
                }

                $tree = new Tree;
                $tree->id_tree = $node[0]->id_tree;
                $tree->node = $value;
                $tree->father = $father;
                $tree->child_left = '';
                $tree->child_rigth = '';
                $tree->root = 0;
                $tree->save(); 
            }

        }

        $root = Tree::where([['root','=', 1],
                                 ['id_tree','=',$treeId]])
                          ->get();
        
        $this->dibujar($root[0]->node,0,$treeId);

        $treeFinal = Tree::where('id_tree','=',$treeId)->get();

        foreach ($treeFinal as $value) {

             $this->levels[$value->level][] = $value->node;
        }
       

        $resp = Array('result' => Array('Sucess' => array ('nodes' =>count($nodes), 'tree' => $this->treeFinal, 'levels' =>$this->levels)), 'error' => null);
        return $response->withJson($resp, 200); 
    }


    public function dibujar($xnodo, $level, $treeId){
          $i=0;
          $aux=0;
          if ($xnodo != ''){

                $nodeNext = Tree::where([['node','=',$xnodo],
                                            ['id_tree','=',$treeId]])->get();

                $this->dibujar($nodeNext[0]->child_rigth, $level+1, $treeId );
                
                for ($i = 1; $i <= $level; $i++)
                    $this->treeFinal .= " \t";

                 
                $this->treeFinal .= $xnodo."\\n";

                $updateRowNode = Tree::where([['node','=',$xnodo],
                                                   ['id_tree','=',$treeId]])
                                        ->update(['level' => $level]);
                 
                $this->dibujar($nodeNext[0]->child_left, $level+1, $treeId);
          }
    }

    public function lowest_common_ancestor (\Slim\Http\Request $request, \Slim\Http\Response $response){

        $id_tree = $request->getAttribute('id_tree');  
        $node1 = $request->getAttribute('node1');  
        $node2 = $request->getAttribute('node2');    

        if (empty($id_tree) || empty($node1) || empty($node2)){
            $resp = array('result' => null, 'error' => 'Value haven\'t been supplied.');
            return $response->withJson($resp, 400);
        }

        $nodes = Tree::whereRaw('id_tree = '.$id_tree.' AND (node = '.$node1.' OR node = '.$node2.')')->get();

        if(count($nodes) != 2){
            $resp = array('result' => null, 'error' => 'Node doesn\'t exist');
            return $response->withJson($resp, 400);
        }

        foreach ($nodes as $node) {
            $recorNodes[] = $node->father;
        }


        $recorNode1 = $recorNodes[0];
        $recorNode2 = $recorNodes[1];
        $father1 = $recorNodes[0];
        $father2 = $recorNodes[1];
        $bandNode1 = true;
        $bandNode2 = true;
        $ancestor = null;

        while($bandNode1){

              $nodeNext = Tree::where([['node','=',$father1],
                                       ['id_tree','=',$id_tree]])->get();

              if($nodeNext[0]->root == 1){
                $bandNode1 = false;
              } else {
                $recorNode1 .= ",".$nodeNext[0]->father;  
                $father1 = $nodeNext[0]->father;
              }

        }

        while($bandNode2){

              $nodeNext = Tree::where([['node','=',$father2],
                                       ['id_tree','=',$id_tree]])->get();

              if($nodeNext[0]->root == 1){
                $bandNode2 = false;
              } else {
                $recorNode2 .= ",".$nodeNext[0]->father;  
                $father2 = $nodeNext[0]->father;
              }

        }

        $arrayNode1 = explode(",",$recorNode1);
        $arrayNode2 = explode(",",$recorNode2);    
        $band = true; 

        foreach ($arrayNode1 as $node1Value) {

            foreach ($arrayNode2 as $node2Value) {
                if($node1Value == $node2Value && $band == true){
                    $ancestor = $node1Value;
                    $band = false;
                }                
            }
        }


        $resp = Array('result' => Array('Sucess' => array('recorNode1' => $recorNode1 , 'recorNode2' => $recorNode2, 'ancestor' => $ancestor), 'error' => null));
        return $response->withJson($resp, 200); ;
    }
    




	
}
