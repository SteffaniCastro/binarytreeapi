<?php

require_once  __DIR__ .'/../vendor/autoload.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Instantiate the app
$settings = require  __DIR__ .'/../src/settings.php';
$app = new \Slim\App($settings);
$appContainer = $app->getContainer();


// Set up definitions.
require  __DIR__ .'/../src/definitions.php';

// Set up dependencies
require  __DIR__ .'/../src/dependencies.php';

// Register routes
require  __DIR__ .'/../src/routes.php';
// Run app
$app->run();

