# Web API Binary Tree

Este documento proporciona pautas y ejemplos para la API Binary Tree. La API Binary Tree tiene como objetivo generar arboles binarios a traves de una cadena de nodos dada.

### Instalación ###

- Git Clone
- Run ```composer update```
- Configurar el ambiente
- Importar base de datos /database/binary_tree.sql

## Request & Response

### EndPoints

  - [POST /api/v1/createTree]
  - [GET /api/v1/lca/{id_tree}/{node1}/{node2}]

### POST /api/v1/createTree

Recibe por parametros una cadena de nodos separados por comas $_POST['nodos']=67,39,76,28,74,85,44,29,83,87

```Ejemplo: http://example/api/v1/createTree```


Response body:

		{
			"result": {
				"Sucess": {
					"nodes": 10,
					"tree": " \t \t \t87\\n \t \t85\\n \t \t \t83\\n \t76\\n \t \t74\\n67\\n \t \t44\\n \t39\\n \t \t \t29\\n \t \t28\\n",
					"levels": [
						[
							67
						],
						[
							39,
							76
						],
						[
							28,
							74,
							85,
							44
						],
						[
							29,
							83,
							87
						]
					]
				}
			},
			"error": null
		}

### GET /api/v1/lca/{id_tree}/{node1}/{node2}

- id_tree: Id del arbol generado
- node1 y node2: Nodos para la busqueda del ancestro en comun

```Ejemplo: http://example/api/v1/lca/3/29/44```


Response body:

	  {
		"result": {
			"Sucess": {
				"recorNode1": "39,67",
				"recorNode2": "28,39,67",
				"ancestor": "39"
			},
			"error": null
		}
	}