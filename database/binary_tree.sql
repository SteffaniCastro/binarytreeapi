-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 09, 2019 at 01:11 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `binary_tree`
--

-- --------------------------------------------------------

--
-- Table structure for table `tree`
--

DROP TABLE IF EXISTS `tree`;
CREATE TABLE IF NOT EXISTS `tree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tree` int(11) NOT NULL,
  `node` int(11) NOT NULL,
  `father` varchar(11) NOT NULL,
  `child_left` varchar(11) NOT NULL,
  `child_rigth` varchar(11) NOT NULL,
  `root` tinyint(4) NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tree`
--

INSERT INTO `tree` (`id`, `id_tree`, `node`, `father`, `child_left`, `child_rigth`, `root`, `level`) VALUES
(1, 1, 80, '', '16', '94', 1, 0),
(2, 1, 16, '80', '', '32', 0, 1),
(3, 1, 94, '80', '86', '97', 0, 1),
(4, 1, 32, '16', '', '64', 0, 2),
(5, 1, 64, '32', '', '70', 0, 3),
(6, 1, 70, '64', '', '', 0, 4),
(7, 1, 86, '94', '', '93', 0, 2),
(8, 1, 97, '94', '', '', 0, 2),
(9, 1, 93, '86', '', '', 0, 3),
(10, 2, 32, '', '16', '48', 1, 0),
(11, 2, 16, '32', '14', '22', 0, 1),
(12, 2, 48, '32', '46', '52', 0, 1),
(13, 2, 52, '48', '51', '60', 0, 2),
(14, 2, 22, '16', '', '', 0, 2),
(15, 2, 14, '16', '8', '', 0, 2),
(16, 2, 8, '14', '', '', 0, 3),
(17, 2, 46, '48', '', '', 0, 2),
(18, 2, 51, '52', '', '', 0, 3),
(19, 2, 60, '52', '', '', 0, 3),
(20, 3, 67, '', '39', '76', 1, 0),
(21, 3, 39, '67', '28', '44', 0, 1),
(22, 3, 76, '67', '74', '85', 0, 1),
(23, 3, 28, '39', '', '29', 0, 2),
(24, 3, 74, '76', '', '', 0, 2),
(25, 3, 85, '76', '83', '87', 0, 2),
(26, 3, 44, '39', '', '', 0, 2),
(27, 3, 29, '28', '', '', 0, 3),
(28, 3, 83, '85', '', '', 0, 3),
(29, 3, 87, '85', '', '', 0, 3);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
